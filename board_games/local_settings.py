# -*- coding: utf8 -*-
# локальный конфиг
DATABASES = {'default': {}}
DATABASES['default']['ENGINE'] = 'django.db.backends.mysql'
DATABASES['default']['NAME'] = ''
DATABASES['default']['USER'] = ''
DATABASES['default']['PASSWORD'] = ''
DATABASES['default']['HOST'] = '127.0.0.1'
DATABASES['default']['PORT'] = '3306'
USE_TZ = False


DEBUG = True
ALLOWED_HOSTS = []

LANGUAGE_CODE = 'ru'
TIME_ZONE = 'UTC'

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

EMAIL_HOST_USER = u''
EMAIL_HOST = u''
EMAIL_HOST_PASSWORD = u''
EMAIL_PORT = 587
EMAIL_USE_TLS = True

CITIES_LIGHT_TRANSLATION_LANGUAGES = ['ru', 'en']
CITIES_LIGHT_INCLUDE_COUNTRIES = ['RU', 'KG', 'KZ', 'UZ']

STATIC_URL = '/static/'
MEDIA_URL = '/media/'