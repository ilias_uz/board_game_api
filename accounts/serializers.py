from rest_framework import serializers
from accounts.models import Users
from cities_light.models import Country, City


class UsersFormSerializers(serializers.ModelSerializer):
    class Meta:
        model = Users
        fields = ('username', 'first_name', 'last_name', 'country', 'city', 'email', 'password')


class CountrySerializers(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = ('id', 'name')


class CitySerializers(serializers.ModelSerializer):
	class Meta:
		model = City
		fields = ('id', 'name', 'country')