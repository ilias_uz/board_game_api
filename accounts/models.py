# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from cities_light.models import Country, City

# Create your models here.
class Users(User):
	country = models.ForeignKey(Country, verbose_name='Страна проживание')
	city = models.ForeignKey(City, verbose_name='Город проживание')
	active = models.BooleanField(verbose_name='Активный', default=True)