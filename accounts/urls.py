# -*- coding: utf-8 -*-
from django.conf.urls import url
from accounts.views import (
	GetCountryList,
	GetCityList,
    UserRegisteration,
	)
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework.authtoken.views import obtain_auth_token 
from django.conf.urls import include


urlpatterns = [
    url(r'^registration/$', UserRegisteration.as_view()), # Регистрация нового пользователя
    url(r'^get_country/$', GetCountryList.as_view()), # Получение списка стран
    url(r'^get_city/$', GetCityList.as_view()), # Получение списка городов
    url(r'^auth_token/$', obtain_auth_token), # Вход принимет имя пользователя и пароль если все хорошо дает токен.
]

urlpatterns = format_suffix_patterns(urlpatterns)