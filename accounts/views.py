# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework import authentication
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework.generics import (
        CreateAPIView,
        DestroyAPIView,
        ListAPIView,
        UpdateAPIView, 
        RetrieveAPIView,
        RetrieveUpdateAPIView,
        ListCreateAPIView
    )
from rest_framework.status import (
        HTTP_201_CREATED,
        HTTP_400_BAD_REQUEST
    )
from rest_framework.permissions import AllowAny

from django.contrib.auth.models import User
from django.shortcuts import render
from django.db.models import Q

from cities_light.models import Country, City
from accounts.serializers import (
		UsersFormSerializers,
		CountrySerializers,
		CitySerializers,
	)
from accounts.models import Users
from rest_framework.filters import SearchFilter

# Create your views here.

class UserRegisteration(CreateAPIView):
    queryset = Users.objects.all()
    serializer_class = UsersFormSerializers
    permission_classes = [AllowAny]

    def post(self, request, format=None):
        serializer = UsersFormSerializers(data=request.data)
        if serializer.is_valid():
            serializer.save()
            password = request.data.get('password')
            username = request.data.get('username')
            user = User.objects.get(username__exact=username)
            user.set_password(password)
            user.save()
            token = Token.objects.create(user=user)
            return Response({"token": token.key}, status=HTTP_201_CREATED)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


class GetCountryList(ListAPIView):
    queryset = Country.objects.all()
    serializer_class = CountrySerializers
    permission_classes = [AllowAny]


class GetCityList(ListAPIView):
    serializer_class = CitySerializers
    filter_backends = [SearchFilter]
    search_fields = ['country', ]
    permission_classes = [AllowAny]

    def get_queryset(self, *args, **kwargs):
        queryset_list = City.objects.all()
        query = self.request.GET.get(u'country_id')
        if query:
            queryset_list = queryset_list.filter(
                    Q(country=query)
                )
        return queryset_list