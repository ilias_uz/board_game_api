# -*- coding: utf-8 -*-
from __future__ import unicode_literals
# Import from Django
from django.db.models import Q
# Import from rest framework
from rest_framework.response import Response
from rest_framework.filters import SearchFilter
from rest_framework.status import (
        HTTP_201_CREATED,
        HTTP_400_BAD_REQUEST
    )
from rest_framework.permissions import (
        AllowAny,
        IsAuthenticated, 
        IsAdminUser,
        IsAuthenticatedOrReadOnly
    )
from rest_framework.generics import (
        CreateAPIView,
        DestroyAPIView,
        ListAPIView,
        UpdateAPIView, 
        RetrieveAPIView,
        RetrieveUpdateAPIView,
        ListCreateAPIView
    )
# Import from application
from board_game_post.serializers import (
        FavoritesGameSerializers, 
        CreateFavoritesGameSerializers,
        BoardGamesCategorySerializers, 
        BoardGameTizerSerializers,
        BoardGameDetailSerializers, 
        BoardGameListSerializers,
        BoardGameCreateSerializers,
        BoardGameUpdateSerializers,
    )
from board_game_post.models import (
        BoardGamesCategory, 
        BoardGame, 
        FavoritesGame
    )
from accounts.models import Users
from board_game_post.permissions import IsOwnerOrReadOnly, IsActive

# Везде проверяется не забанил ли админ пользователя

# Получение листа категории настольных
class ListBoardGamesCategory(ListAPIView):
    queryset = BoardGamesCategory.objects.all()
    permission_classes = [IsAuthenticated, IsActive]
    serializer_class = BoardGamesCategorySerializers


# Создание нового поста 
class CreateBoardGame(CreateAPIView):
    queryset = BoardGame.objects.all()
    permission_classes = [IsAuthenticated, IsActive]
    serializer_class = BoardGameCreateSerializers

    def perform_create(self, serializer):
        try:
            user = Users.objects.get(username=self.request.user)
        except:
            user = self.request.user
        serializer.save(user=user)


# Получение одного из постов
class DetailBoardGame(RetrieveAPIView):
    queryset = BoardGame.objects.all()
    permission_classes = [IsAuthenticated, IsActive]
    serializer_class = BoardGameDetailSerializers


# Обновление одного из постов
class UpdateBoardGame(RetrieveUpdateAPIView):
    queryset = BoardGame.objects.all()
    permission_classes = [IsOwnerOrReadOnly, IsActive]
    serializer_class = BoardGameUpdateSerializers

    def perform_create(self, serializer):
        try:
            user = Users.objects.get(username=self.request.user)
        except:
            user = self.request.user
        serializer.save(user=user)


# Удаление одного из постов
class DeleteBoardGame(DestroyAPIView):
    queryset = BoardGame.objects.all()
    permission_classes = [IsOwnerOrReadOnly, IsActive]
    serializer_class = BoardGameListSerializers
    

# Список постов и получение поста по категориям
class ListBoardGame(ListAPIView):
    permission_classes = [IsAuthenticated, IsActive]
    serializer_class = BoardGameListSerializers
    filter_backends = [SearchFilter]
    search_fields = ['category', ]

    def get_queryset(self, *args, **kwargs):
        queryset_list = BoardGame.objects.all()
        query = self.request.GET.get(u'category_id')
        if query:
            queryset_list = queryset_list.filter(
                    Q(category=query)
                )
        return queryset_list


# Тизер одного из постов
class TizerBoardGame(RetrieveAPIView):
    queryset = BoardGame.objects.all()
    permission_classes = [IsAuthenticated, IsActive]
    serializer_class = BoardGameTizerSerializers


# Создание избранного поста
class CreateFavoriteGames(CreateAPIView):
    permission_classes = [IsAuthenticated, IsActive]
    serializer_class = CreateFavoritesGameSerializers

    def perform_create(self, serializer):
        post = BoardGame.objects.get(pk=self.kwargs['pk'])
        try:
            user = Users.objects.get(username=self.request.user)
        except:
            user = self.request.user
        favorite = FavoritesGame.objects.filter(user=user, post=post).first()
        if not favorite:
            serializer.save(user=user, post=post)


# Получение списка избранных постов
class ListFavoriteGames(ListAPIView):
    permission_classes = [IsAuthenticated, IsActive]
    serializer_class = FavoritesGameSerializers

    def get_queryset(self, *args, **kwargs):
        queryset_list = FavoritesGame.objects.all()
        queryset_list = queryset_list.filter(
                Q(user=self.request.user)
            )
        return queryset_list

# Удаление избранного поста
class DeleteFavoriteGames(DestroyAPIView):
    queryset = FavoritesGame.objects.all()
    permission_classes = [IsAuthenticated, IsOwnerOrReadOnly, IsActive]
    serializer_class = FavoritesGameSerializers


