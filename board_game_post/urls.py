# -*- coding: utf-8 -*-
from django.conf.urls import url, include
from board_game_post.views import (
	CreateBoardGame,
	DeleteBoardGame,
	DetailBoardGame,
	TizerBoardGame,
	ListBoardGame,
	UpdateBoardGame,
	ListBoardGamesCategory,
	ListFavoriteGames,
	CreateFavoriteGames,
	DeleteFavoriteGames,
	)
from rest_framework.urlpatterns import format_suffix_patterns

# Url для подключение к api restframefork. Удаление,создание, редактирование.
urlpatterns = [
    url(r'^category/$', ListBoardGamesCategory.as_view()), # Лист категории
    url(r'^post/(?P<pk>[0-9]+)/$', DetailBoardGame.as_view(), name='game_detail'), # Получение одного поста
    url(r'^post/(?P<pk>[0-9]+)/tizer/$', TizerBoardGame.as_view(), name='tizer'), # Получение тизера поста
    url(r'^post/$', ListBoardGame.as_view(), name='game_list'), # Получение список постов
    url(r'^post/add/$', CreateBoardGame.as_view(), name='create_game'), # Добавление нового поста
    url(r'^post/(?P<pk>[0-9]+)/edit/$', UpdateBoardGame.as_view(), name='game_update'), # Обновление поста
    url(r'^post/(?P<pk>[0-9]+)/delete/$', DeleteBoardGame.as_view(), name='game_delete'), # Удаление поста
    url(r'^favorite_posts_add/(?P<pk>[0-9]+)/$', CreateFavoriteGames.as_view()), # Создание избранного поста по id поста
    url(r'^favorite_posts/(?P<pk>[0-9]+)/delete/$', DeleteFavoriteGames.as_view()), # Удаление избраного поста
    url(r'^favorite_posts/$', ListFavoriteGames.as_view()), # Избранные посты список
]

urlpatterns = format_suffix_patterns(urlpatterns)