# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from models import BoardGamesCategory, BoardGame, FavoritesGame

admin.site.register(BoardGamesCategory)
admin.site.register(BoardGame)
admin.site.register(FavoritesGame)

# Register your models here.
