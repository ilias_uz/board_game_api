# -*- coding: utf-8 -*-
from rest_framework import serializers
from board_game_post.models import BoardGamesCategory, BoardGame, FavoritesGame

# Serializer модели для создание редактирование и удаление постов, избранные
class BoardGamesCategorySerializers(serializers.ModelSerializer):
    class Meta:
        model = BoardGamesCategory
        fields = ('id', 'category')


class BoardGameDetailSerializers(serializers.ModelSerializer):
    class Meta:
        model = BoardGame
        fields = ('id', 'game_name', 'under_hedline', 'preview', 'post_image', 'game_description', 'category', 'user', 'status')


class BoardGameUpdateSerializers(serializers.ModelSerializer):
    class Meta:
        model = BoardGame
        fields = ('id', 'game_name', 'under_hedline', 'preview', 'post_image', 'game_description', 'category', 'user', 'status')


class BoardGameListSerializers(serializers.ModelSerializer):
    class Meta:
        model = BoardGame
        fields = ('id', 'game_name', 'under_hedline')


class BoardGameCreateSerializers(serializers.ModelSerializer):
    class Meta:
        model = BoardGame
        fields = ('id', 'game_name', 'under_hedline', 'preview', 'post_image', 'game_description', 'category', 'status')


class BoardGameTizerSerializers(serializers.ModelSerializer):
    class Meta:
        model = BoardGame
        fields = ('id', 'game_name', 'under_hedline', 'preview', 'post_image')


class FavoritesGameSerializers(serializers.ModelSerializer):
    class Meta:
        model = FavoritesGame
        fields = ('id', 'user', 'post')


class CreateFavoritesGameSerializers(serializers.ModelSerializer):
    class Meta:
        model = FavoritesGame
        fields = ('id', )

