# -*- coding: utf-8 -*-
from rest_framework.permissions import BasePermission
from accounts.models import Users

# Определение автора поста 
class IsOwnerOrReadOnly(BasePermission):
	message = u'Вы не являетесь автором этого поста!'

	def has_object_permission(self, request, view, obj):
		try:
			user = Users.objects.get(username=request.user)
		except:
			user = request.user
		# or request.user.is_superuser
		return obj.user == user

# Проверка не были отключено аккаунт админом
class IsActive(BasePermission):
	message = u'Ваш аккаунт отключен!'

	def has_permission(self, request, view):
		user = Users.objects.get(username=request.user)
		if user.active == True:
			return user
		else:
			return not user