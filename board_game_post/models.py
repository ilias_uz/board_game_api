# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from accounts.models import Users

from board_game_post.image_resizeable import ResizedImageField

# Категория для поста
class BoardGamesCategory(models.Model):
	category = models.CharField(max_length=200, verbose_name='Катигория')

	def __unicode__(self):
		return self.category

# Модель самого поста
class BoardGame(models.Model):
	game_name = models.CharField(max_length=200, help_text='Напишите заголовок поста', verbose_name='Заголовок')
	under_hedline = models.CharField(max_length=500, help_text='Напишите подзаголовок', verbose_name='Подзаголовок')
	preview = models.TextField(help_text='Несколько строчек анонса', verbose_name='Анонса')
	post_image = ResizedImageField(upload_to='media/', help_text='Выберите фото для поста', verbose_name='Фото пота')
	game_description = models.TextField(help_text='Тело пост', verbose_name='Описание')
	category = models.ForeignKey(BoardGamesCategory, verbose_name='Катигории')
	user = models.ForeignKey(Users, verbose_name='Автор поста')
	status = models.BooleanField(verbose_name='Опубликовано')

	def __unicode__(self):
		return self.game_name

# Модель избранные посты настольных игр
class FavoritesGame(models.Model):
	user = models.ForeignKey(Users, verbose_name='Пользователь')
	post = models.ForeignKey(BoardGame, verbose_name='Пост', help_text='Избранный пост')

	def __unicode__(self):
		return self.user.username
		