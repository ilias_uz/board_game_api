# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.core.mail import EmailMessage
from django.core.mail import send_mail
from django.conf import settings
from django.db.models import Q

from user_admin.serializers import (
		BoardGamesCategorySerializers,
		BoardGameUpdateSerializers,
		UserStatusSerializers,
		UsersSerializers,
	)
# Import from application
from rest_framework.generics import (
        CreateAPIView,
        DestroyAPIView,
        ListAPIView,
        UpdateAPIView, 
        RetrieveAPIView,
        RetrieveUpdateAPIView,
        ListCreateAPIView
    )
from rest_framework.status import (
        HTTP_201_CREATED,
        HTTP_400_BAD_REQUEST
    )
from rest_framework.permissions import (
        AllowAny,
        IsAuthenticated, 
        IsAdminUser,
        IsAuthenticatedOrReadOnly
    )
from rest_framework.response import Response
from rest_framework.permissions import IsAdminUser
from rest_framework.filters import SearchFilter

from board_game_post.models import BoardGamesCategory, BoardGame
from board_game_post.permissions import IsOwnerOrReadOnly
from board_game_post.serializers import (
        BoardGameDetailSerializers, 
        BoardGameListSerializers,
    )

from accounts.models import Users

# Create your views here.

# Создание поста
class CreateBoardGamesCategory(CreateAPIView):
    queryset = BoardGamesCategory.objects.all()
    permission_classes = [IsAdminUser]
    serializer_class = BoardGamesCategorySerializers

    def post(self, request, format=None):
        serializer = BoardGamesCategorySerializers(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=HTTP_201_CREATED)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


# Обновление статуса поста
class UpdateBoardGame(RetrieveUpdateAPIView):
    queryset = BoardGame.objects.all()
    permission_classes = [IsAdminUser]
    serializer_class = BoardGameUpdateSerializers

    # Отправка письма в почту создателя поста от системы 
    def perform_update(self, serializer):
		instance = serializer.save()
		user = Users.objects.get(pk=instance.user.id)
		if instance.status == False:
			status = 'Не опубликован.'
		elif instance.status == True:
			status = 'Опубликован.'
		subject = 'Пост ' + instance.game_name
		message = 'Пост: ' + instance.game_name + '\n\n\n' + 'Статус поста админом было изминено на '+status
		send_mail(subject, message, settings.EMAIL_HOST_USER, [user.email], fail_silently=False)


# Бан или активирование пользователей
class UpdateUsersActive(RetrieveUpdateAPIView):
    queryset = Users.objects.all()
    permission_classes = [IsAdminUser]
    serializer_class = UserStatusSerializers

    # Отправка письмо пользователю о бане или активации аккаунта
    def perform_update(self, serializer):
		instance = serializer.save()
		if instance.active == False:
			status = 'отключено.'
		elif instance.active == True:
			status = 'активировано.'
		subject = 'Пользователю ' + instance.username
		message = 'Аккаунт: ' + instance.username + '\n\n\n' + 'Аккаунт было '+status
		send_mail(subject, message, settings.EMAIL_HOST_USER, [instance.email], fail_silently=False)


# Список пользователей
class ListUsers(ListAPIView):
    queryset = Users.objects.all()
    permission_classes = [IsAdminUser]
    serializer_class = UsersSerializers


# Получение листа категории настольных
class ListBoardGamesCategory(ListAPIView):
    queryset = BoardGamesCategory.objects.all()
    permission_classes = [IsAdminUser]
    serializer_class = BoardGamesCategorySerializers


# Получение одного из постов
class DetailBoardGame(RetrieveAPIView):
    queryset = BoardGame.objects.all()
    permission_classes = [IsAdminUser]
    serializer_class = BoardGameDetailSerializers


# Список постов и получение поста по категориям
class ListBoardGame(ListAPIView):
    permission_classes = [IsAdminUser]
    serializer_class = BoardGameListSerializers
    filter_backends = [SearchFilter]
    search_fields = ['category', ]

    def get_queryset(self, *args, **kwargs):
        queryset_list = BoardGame.objects.all()
        query = self.request.GET.get(u'category_id')
        if query:
            queryset_list = queryset_list.filter(
                    Q(category=query)
                )
        return queryset_list
    