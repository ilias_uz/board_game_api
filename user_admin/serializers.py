from rest_framework import serializers
from board_game_post.models import BoardGamesCategory, BoardGame
from accounts.models import Users

class BoardGamesCategorySerializers(serializers.ModelSerializer):
    class Meta:
        model = BoardGamesCategory
        fields = ('id', 'category')


class BoardGameUpdateSerializers(serializers.ModelSerializer):
    class Meta:
        model = BoardGame
        fields = ('id', 'status')


class UsersSerializers(serializers.ModelSerializer):
    class Meta:
        model = Users
        fields = ('id', 'username', 'active')


class UserStatusSerializers(serializers.ModelSerializer):
    class Meta:
        model = Users
        fields = ('active', )


