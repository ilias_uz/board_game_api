# -*- coding: utf-8 -*-
from django.conf.urls import url, include
from user_admin.views import (
		CreateBoardGamesCategory,
		UpdateBoardGame,
		ListUsers,
		UpdateUsersActive,
		ListBoardGamesCategory,
		ListBoardGame,
		DetailBoardGame,
	)
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework.authtoken.views import obtain_auth_token 


urlpatterns = [
    url(r'^category/add/$', CreateBoardGamesCategory.as_view()), # Добавление категории
    url(r'^category/$', ListBoardGamesCategory.as_view()), # Получение всех категории
    url(r'^post/$', ListBoardGame.as_view(), name='game_list'), # Получение списка постов и по id катигориям /?category_id=id
    url(r'^post/(?P<pk>[0-9]+)/$', DetailBoardGame.as_view(), name='game_detail'), # Получение одного поста
    url(r'^post/(?P<pk>[0-9]+)/edit/$', UpdateBoardGame.as_view(), name='game_update'), # Изминение статус поста
    url(r'^user_active/(?P<pk>[0-9]+)/$', UpdateUsersActive.as_view(), name='user_active'), # Банить или активировать
    url(r'^user_active/$', ListUsers.as_view(), name='users_list'), # Получение списка пользователей
]

urlpatterns = format_suffix_patterns(urlpatterns)