API Для создание/изминение/удаление постов о настольных игр и управлением пользователям. Использовалось DjangoRestFreamWorkApi.

Данное api rest freamwork разделено на три части:
	1)Accounts - Создание аккаунта, получение списка городов и стран, вход которое возврашает token через которого пользователь сможет работать с приложением
	2)Board_Game_Post - Создание постов, редактирование
	3)user_admin - Для админ которое является суппер пользователям

Перед началом запуска API нужно выполнить несколько шагов:
	1) Создайте виртуальное окружение с помощю virtualenv и активируйте
		$ virtualenv <название окружение>
		$ source <название окружение>/bin/activate
	2) Установите зависимости которое находится внутри проекта
		$ pip install -r requirements.txt
	3) Создайте новую базу данных mysql, пользователя, и передайте все превилегие базу данных к созданному пользователю 
		$ mysql -u <имя пользователя> -p
		введите пароль:
		mysql> CREATE DATABASE <название бд> CHARACTER SET UTF8;
		mysql> CREATE USER <имя пользователя>@localhost IDENTIFIED BY '<пароль>';
		mysql> GRANT ALL PRIVILEGES ON <название бд>.* TO <имя пользователя>@localhost;
		mysql> FLUSH PRIVILEGES;
	4) Обновите созданную вами базу данных с помошью файла apidb.sql которое находится внутри проекта
		$ mysql -u <имя пользователя> -p
		введите пароль:
		mysql> use <название бд>;
		mysql> source <Путь к apidb.sql>;
	5) Зайдите в board_games/local_settings.py и настройте базу данных по параметрам которых вы вели ране
	6) Выпольниете миграцию
		$ chmod +x manage.py
		$ ./manage.py migrate
	7) Загрузите стран, и городов
		$ ./manage.py cities_light --force-all - Не много долго качает список стран, и городов придется не много подождать 10-15 мин пока не загрузился все что надо.
	8) Создайте супер пользователя 
		$ ./manage.py createsuperuser
	9) Настройте данные EMAIL для отапрвки письма пользователям

Если все прошло успешно то тогда запустите проект для использование api
$ ./manage.py runserver



